# React tutorial Tic Tac Toe

Follow the react tutorial on [reactjs.org](https://reactjs.org/tutorial/tutorial.html#setup-option-2-local-development-environment)

This repo covers the setup part of the tutorial.


### Setup

 - Install [Node](https://nodejs.org/en/download/), preferably on linux/mac or wsl.
 - Start a terminal, If using Windows without wsl, start git bash or a bash capable terminal
 - Clone this repo.
 - Navigate to the directory you cloned in your terminal.
 - Then run  `npm install`.
 - Then run `npm start` to compile your js and css.

# Support

Contact devs at Zpirit for help.

Øyvind Eikeland oyvind@zpirit.no

Roar Helland roar@zpirit.no

Espen Kvalheim espen@zpirit.no
